/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gpsextractor;

import com.gpsextractor.EXIFReader.ImageInfo;
import com.gpsextractor.imageResizer.SizeType;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 *
 * @author Trader
 */
public abstract class WizardControl implements EventHandler<OnActivateEvent>{

    protected static List<ImageInfo> imageInfos = new ArrayList<>();
    
    protected static Stage stage;
    
    protected static File imageDirectory;
    protected static File previewDirectory;
    protected static String kmlFileName;
    
    protected static SizeType selectedSizeTypePreview = SizeType.PROZENT;
    protected static Double sizeValuePreview = 2.0;
    protected static Boolean overrideImagesIfExist = false;
    
    protected static SizeType selectedSizeTypeKMLBubble = SizeType.ABSOLUTE_WIDTH;
    protected static Double sizeValueKMLBubble = 1000.0;
    
}
