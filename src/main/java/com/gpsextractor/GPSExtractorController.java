package com.gpsextractor;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Trader
 */
public class GPSExtractorController extends WizardControl implements Initializable{

    @FXML
    private AnchorPane content;
    
    protected static Button nextButton;
    protected static Button backButton;
    protected static Button quitButton;
    
    @FXML private Button myNextButton;
    @FXML private Button myBackButton;
    @FXML private Button myQuitButton;
    
    private List<AnchorPane> pages = new LinkedList<>();
    private ListIterator<AnchorPane> pagesInterator;
    private boolean lastDirectionForward = true;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        nextButton = myNextButton;
        backButton = myBackButton;
        quitButton = myQuitButton;
        
        try {
            pages.add(FXMLLoader.load(getClass().getResource("/Intro.fxml")));
            pages.add(FXMLLoader.load(getClass().getResource("/SelectDirectory.fxml")));
            pages.add(FXMLLoader.load(getClass().getResource("/ImageResizer.fxml")));
            pages.add(FXMLLoader.load(getClass().getResource("/KMLOptionPage.fxml")));
            pages.add(FXMLLoader.load(getClass().getResource("/KMLGenerator.fxml")));
        } catch (IOException ex) {
            System.err.println("ERROR: " + ex.getMessage());
            ex.printStackTrace();
        }

        pagesInterator = pages.listIterator();
        loadNext();
        
    }    
    
    @FXML
    private void loadNext(){
        if(!lastDirectionForward) pagesInterator.next();
        if(pagesInterator.hasNext()){
            AnchorPane next = pagesInterator.next();
            content.getChildren().setAll(next);
            next.fireEvent(new OnActivateEvent());
        }
        lastDirectionForward = true;
        
    }
    
    @FXML
    private void loadLast(){
        if(lastDirectionForward) pagesInterator.previous();
        if(pagesInterator.hasPrevious()){
            AnchorPane previous = pagesInterator.previous();
            content.getChildren().setAll(previous);
            previous.fireEvent(new OnActivateEvent());
        }
        lastDirectionForward = false;
    }
    
    @FXML
    private void quit(){
        System.exit(0);
    }

    @Override
    public void handle(OnActivateEvent t) {}
    
    
}
