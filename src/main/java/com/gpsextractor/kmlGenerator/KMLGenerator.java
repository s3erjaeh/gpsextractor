/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gpsextractor.kmlGenerator;

import com.gpsextractor.EXIFReader.ImageInfo;
import com.gpsextractor.imageResizer.SizeType;
import java.io.*;
import org.imgscalr.Scalr;
import static org.imgscalr.Scalr.resize;
import org.jdom2.*;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.*;

/**
 *
 * @author Admin
 */
public class KMLGenerator {

    private Document document;
    private Element root;
    private Element placemarkParent;
    private final SAXBuilder parser = new SAXBuilder();
    private File previewDirectory;

    public KMLGenerator() {
    }

    public void createDocument(String tripName, File previewDirectory) {
        
        document = new Document();
        root = new Element("kml");
        placemarkParent = new Element("Document");
        Element name = new Element("name");
        
        name.setText(tripName);
        placemarkParent.addContent(name);
        root.addContent(placemarkParent);
        document.setRootElement(root);
        this.previewDirectory = previewDirectory;
    }

    private Element createStyleElement(ImageInfo info)
    {
        Element style = new Element("Style");
        Element iconStyle = new Element("IconStyle");
        Element icon = new Element("Icon");
        Element href = new Element("href");
        
        href.setText(previewDirectory.getName() + "/" + info.getFile().getName());
        icon.addContent(href);
        iconStyle.addContent(icon);
        style.addContent(iconStyle);
        
        return style;
                  
    }
    
    private Element createDescription(ImageInfo info, SizeType type, double factor) {
        Element result = new Element("description");
        Element html = new Element("html");
        Element body = new Element("body");
        Element div = new Element("div");
        Element img = new Element("img");

        
        int newHeight = info.getImageHeight();
        int newWidth  = info.getImageWidth();
        switch (type){
            case PROZENT:{
                factor /= 100.0;
            }
            case FACTOR:{
                newHeight *= factor;
                newWidth *= factor;
            }break;
            case ABSOLUTE_HEIGHT: {
                newHeight = (int) factor;
                newWidth *= ((double) newHeight / (double) info.getImageHeight());
            } break;
            case ABSOLUTE_WIDTH:{
                newWidth = (int) factor;
                newHeight *= ((double) newWidth / (double) info.getImageWidth());
            }break;
        }        
        
        //Attribute imgId = new Attribute("id", info.getFile().getName().replace(".jpg", "").replace(".JPG", ""));
        Attribute imgSrc = new Attribute("src", info.getFile().getName().replace(".JPG", ".jpg"));
        Attribute imgwidth = new Attribute("width", String.valueOf(newWidth));
        Attribute imgheight = new Attribute("height", String.valueOf(newHeight));
        //Attribute imgOnClick = new Attribute("onclick", "onImageClick()");

        //img.setAttribute(imgId);
        img.setAttribute(imgSrc);
        img.setAttribute(imgwidth);
        img.setAttribute(imgheight);
        //img.setAttribute(imgOnClick);

        div.addContent(new Element("span").setText(info.getOrt() + ", " + info.getLandkreis() + ", " + info.getLand()));
        div.addContent(new Element("br"));
        div.addContent(new Element("span").setText("Reisetag: " + info.getReisetag() + ": " + info.getSehenswürdigkeit()));

        div.addContent(img);
        body.addContent(div);
        html.addContent(body);
        result.addContent(html);

        return result;

    }

    public void addImage(ImageInfo info,SizeType type, Double size) {
        Element placemark = new Element("Placemark");
        Element name = new Element("name");
        //Element styleUrl = new Element("styleUrl");
        Element lookAt = new Element("LookAt");
        Element longitude = new Element("longitude");
        Element latitude = new Element("latitude");
        Element point = new Element("Point");
        Element coordinates = new Element("coordinates");

        //name.setText(info.getName().getParentFile().getName() + "\\" + info.getName().getName());
        name.setText(info.getFile().getName());
        //styleUrl.setText("#photoIcon");

        longitude.setText(String.valueOf(info.getLocation().getLongitude()));
        latitude.setText(String.valueOf(info.getLocation().getLatitude()));

        lookAt.addContent(longitude);
        lookAt.addContent(latitude);
        lookAt.addContent(new Element("heading").setText("0.000000"));
        lookAt.addContent(new Element("range").setText("800"));
        lookAt.addContent(new Element("tilt").setText("45"));

        coordinates.setText(info.getLocation().getLongitude() + "," + info.getLocation().getLatitude());
        point.addContent(coordinates);

        placemark.addContent(name);
        placemark.addContent(createDescription(info,type,size));
        //placemark.addContent(styleUrl);
        placemark.addContent(createStyleElement(info));
        placemark.addContent(lookAt);
        placemark.addContent(point);

        placemarkParent.addContent(placemark);

    }

    public void writeFile(String filename) {

        FileOutputStream stream = null;
        try {
            XMLOutputter output = new XMLOutputter(Format.getPrettyFormat());
            stream = new FileOutputStream(filename);
            output.output(document, stream);
        } catch (IOException ex) {
        } finally {
            try {
                stream.close();
            } catch (IOException ex) {
            }
        }
    }
}
