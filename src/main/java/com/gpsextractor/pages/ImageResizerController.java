/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gpsextractor.pages;

import com.gpsextractor.EXIFReader.ImageInfo;
import com.gpsextractor.GPSExtractorController;
import com.gpsextractor.OnActivateEvent;
import com.gpsextractor.WizardControl;
import com.gpsextractor.imageResizer.ImageResizer;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Trader
 */
public class ImageResizerController extends GPSExtractorController implements Initializable {

    @FXML AnchorPane root;
    @FXML Label currentImageLabel;
    @FXML ProgressBar imageProgressBar;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        root.addEventHandler(OnActivateEvent.ACTIVATE, this);
    }    

    @Override
    public void handle(OnActivateEvent t) {
        nextButton.setDisable(true);
        backButton.setDisable(false);
        
        final File[] images = imageDirectory.listFiles((File dir, String name) -> name.toLowerCase().endsWith(".jpg"));
        for (File image : images) {
            imageInfos.add(new ImageInfo(image));
        }

        previewDirectory.mkdirs();
        
        ImageResizer resizer = new ImageResizer();
        
        imageProgressBar.progressProperty().bind(resizer.getProgressProperty());
        currentImageLabel.textProperty().bind(resizer.currentFileStringProperty());
        
        resizer.setOnSuccess(event -> {
            nextButton.setDisable(false);
        });
        
        quitButton.setOnAction(event -> {
            resizer.cancle();
            System.exit(0);
        });

        resizer.resizeImages(imageInfos, previewDirectory,selectedSizeTypePreview, sizeValuePreview,overrideImagesIfExist);
    }
    
}
