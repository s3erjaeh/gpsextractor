/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gpsextractor.pages;

import com.gpsextractor.EXIFReader.EXIFReader;
import com.gpsextractor.EXIFReader.ImageInfo;
import com.gpsextractor.GPSExtractorController;
import com.gpsextractor.OnActivateEvent;
import com.gpsextractor.WizardControl;
import com.gpsextractor.kmlGenerator.KMLGenerator;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Trader
 */
public class KMLGeneratorController extends GPSExtractorController implements Initializable {
    
    @FXML AnchorPane root;
    @FXML Label currentImageLabel;
    @FXML ProgressBar imageProgressBar;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        root.addEventHandler(OnActivateEvent.ACTIVATE, this);
    }    

    @Override
    public void handle(OnActivateEvent t) {
        nextButton.setDisable(true);
        nextButton.setText("Fertigstellen");
        backButton.setDisable(false);
        
        File kmlFile = new File(imageDirectory.getPath() + "/" +  kmlFileName + ".kml");
         
        EXIFReader reader = new EXIFReader();
        
        imageProgressBar.progressProperty().bind(reader.getProgressProperty());
        currentImageLabel.textProperty().bind(reader.currentFileStringProperty());
        
        reader.setOnSuccess(event -> {
            KMLGenerator generator = new KMLGenerator();
            generator.createDocument(kmlFileName,previewDirectory);
            for (ImageInfo imageInfo : imageInfos) {
                generator.addImage(imageInfo,selectedSizeTypeKMLBubble,sizeValueKMLBubble);
            }
            generator.writeFile(kmlFile.getPath());
            
            nextButton.setDisable(false);
        });
        
        nextButton.setOnAction(event -> {
            System.exit(0);
        });
        
        quitButton.setOnAction(event -> {
            reader.cancle();
            System.exit(0);
        });

        reader.readEXIF(imageInfos, kmlFile);
    }
}
