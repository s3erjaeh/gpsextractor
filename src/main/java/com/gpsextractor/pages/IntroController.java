/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gpsextractor.pages;

import com.gpsextractor.GPSExtractorController;
import com.gpsextractor.OnActivateEvent;
import com.gpsextractor.WizardControl;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Trader
 */
public class IntroController  extends GPSExtractorController implements Initializable {

    @FXML AnchorPane root;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        root.addEventHandler(OnActivateEvent.ACTIVATE, this);
    }        
    
    @Override
    public void handle(OnActivateEvent t) {
        nextButton.setDisable(false);
        backButton.setDisable(true);
    }
}
