/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gpsextractor.pages;

import com.gpsextractor.GPSExtractorController;
import com.gpsextractor.OnActivateEvent;
import com.gpsextractor.WizardControl;
import com.gpsextractor.imageResizer.SizeType;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.Pattern;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;

/**
 * FXML Controller class
 *
 * @author Trader
 */
public class SelectDirectoryController extends GPSExtractorController implements Initializable {

    @FXML private AnchorPane root;
    @FXML private TextField directoryTextField;
    @FXML private ListView<String> fileList;
    @FXML private ComboBox<SizeType> selectSizeTypeCombo;
    @FXML private TextField selectSizeTextField;
    @FXML private Label selectSizeLabel;
    @FXML private TextField previewFolderTextField;
    @FXML private CheckBox overrideImagesIfExistCheckbox;
    
    private DirectoryChooser chooser = new DirectoryChooser();
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        root.addEventHandler(OnActivateEvent.ACTIVATE, this);
        
        for (SizeType value : SizeType.values()) {
            selectSizeTypeCombo.getItems().add(value);
        };
        selectSizeTypeCombo.setValue(selectedSizeTypePreview);
        selectSizeTextField.setText(String.valueOf(sizeValuePreview));
        overrideImagesIfExistCheckbox.setSelected(overrideImagesIfExist);
        
        previewFolderTextField.textProperty().addListener((ObservableValue<? extends String> ov, String old, String newValue) -> {
            previewDirectory = new File(newValue);
            checkFormularComplete();
        });
        
        selectSizeTextField.textProperty().addListener((ObservableValue<? extends String> ov, String t, String newValue) -> {
            sizeSelected(newValue);
        });
        
        directoryTextField.textProperty().addListener((ObservableValue<? extends String> ov, String t, String newValue) -> {
            updateImageDirectory(newValue);
        });
        overrideImagesIfExistCheckbox.setOnAction(event -> {
            overrideImagesIfExist = overrideImagesIfExistCheckbox.isSelected();
        });
    }        
    
    @FXML
    private void selectDirectory(){
        if(null != imageDirectory)
            chooser.setInitialDirectory(imageDirectory);
        imageDirectory = chooser.showDialog(root.getScene().getWindow());
        if(null != imageDirectory){
            directoryTextField.setText(imageDirectory.getPath());
        }
    }
    
    @FXML private void selectPreviewDirectory(){
        if(null != previewDirectory)
            chooser.setInitialDirectory(previewDirectory);
        previewDirectory = chooser.showDialog(root.getScene().getWindow());
        if(null != previewDirectory){
            previewFolderTextField.setText(previewDirectory.getPath());
        }
    }
    
    private void updateImageDirectory(String value){
        if(!value.isEmpty()){
            imageDirectory = new File(directoryTextField.getText());
            if(!imageDirectory.exists()){
                directoryTextField.setStyle("-fx-background-color: red;");
                fileList.itemsProperty().get().clear();
                imageDirectory = null;
                checkFormularComplete();
            }
            else{
                directoryTextField.setStyle("-fx-background-color: white;");
                
                for (File f : imageDirectory.listFiles((File dir, String name) -> name.toLowerCase().endsWith(".jpg")))
                {
                    fileList.itemsProperty().get().add(f.getName());
                }
                previewDirectory = new File(imageDirectory.getPath().concat("/preview"));
                previewFolderTextField.setText(previewDirectory.getPath());
            }
        }
        checkFormularComplete();
    }
    
    
    @FXML private void onSizeTypeSelected(){
        selectedSizeTypePreview = selectSizeTypeCombo.getValue();
        selectSizeLabel.setText(selectedSizeTypePreview.getUnit());
    }
    
    private void checkFormularComplete(){
        if(! ((null == previewDirectory) || (null == imageDirectory) || (sizeValuePreview == null))){
            nextButton.setDisable(false);
        }
        else nextButton.setDisable(true);
    }

    private void sizeSelected(String value){
        sizeValuePreview = null;
        switch(selectSizeTypeCombo.getValue()){
            case PROZENT: {
                if(Pattern.matches("[0-9]+", value)) sizeValuePreview =  trim(1,200,Double.valueOf(value));
            }break;
            case ABSOLUTE_HEIGHT:
            case ABSOLUTE_WIDTH:
            {
                if(Pattern.matches("[0-9]+", value)) sizeValuePreview =  trim(1,Double.MAX_VALUE,Double.valueOf(value));
            }break;
            case FACTOR:{
                if(Pattern.matches("[.0-9]+", value)) sizeValuePreview =  trim(0.00000001,1.0,Double.valueOf(value));
            }break;
            default:sizeValuePreview = null;
        }
        checkFormularComplete();

    }
    
    private double trim(double min, double max, double value){
        if(value > max) return max;
        if(value < min) return min;
        return value;
    }
    
    @Override
    public void handle(OnActivateEvent t) {
        backButton.setDisable(false);
        checkFormularComplete();
    }
    
    

}
