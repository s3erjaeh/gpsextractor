/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gpsextractor.pages;

import com.gpsextractor.GPSExtractorController;
import com.gpsextractor.OnActivateEvent;
import com.gpsextractor.WizardControl;
import com.gpsextractor.imageResizer.SizeType;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.Pattern;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Trader
 */
public class KMLOptionPageController extends GPSExtractorController implements Initializable {

    @FXML AnchorPane root;
    @FXML private ComboBox<SizeType> selectSizeTypeCombo;
    @FXML private TextField kmlNameTextField;
    @FXML private TextField selectSizeTextField;
    @FXML private Label selectSizeLabel;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        root.addEventHandler(OnActivateEvent.ACTIVATE, this);
        
        for (SizeType value : SizeType.values()) {
            selectSizeTypeCombo.getItems().add(value);
        };
        selectSizeTypeCombo.setValue(selectedSizeTypeKMLBubble);
        selectSizeTextField.setText(String.valueOf(sizeValueKMLBubble));
        selectSizeLabel.setText(selectedSizeTypeKMLBubble.getUnit());
        
        selectSizeTextField.textProperty().addListener((ObservableValue<? extends String> ov, String t, String newValue) -> {
            sizeSelected(newValue);
        });
        kmlNameTextField.textProperty().addListener((ObservableValue<? extends String> ov, String t, String newValue) -> {
            kmlFileName = newValue;
        });
    }    

    @Override
    public void handle(OnActivateEvent t) {
        backButton.setDisable(false);
        nextButton.setDisable(false);
        nextButton.setText("Weiter");
        
        if(kmlFileName == null) kmlFileName = imageDirectory.getName();
        kmlNameTextField.setText(kmlFileName);
    }
    
    @FXML private void onSizeTypeSelected(){
        selectedSizeTypeKMLBubble = selectSizeTypeCombo.getValue();
        selectSizeLabel.setText(selectedSizeTypeKMLBubble.getUnit());
    }

    private void sizeSelected(String value){
        sizeValueKMLBubble = null;
        switch(selectSizeTypeCombo.getValue()){
            case PROZENT: {
                if(Pattern.matches("[0-9]+", value)) sizeValueKMLBubble =  trim(1,200,Double.valueOf(value));
            }break;
            case ABSOLUTE_HEIGHT:
            case ABSOLUTE_WIDTH:
            {
                if(Pattern.matches("[0-9]+", value)) sizeValueKMLBubble =  trim(1,Double.MAX_VALUE,Double.valueOf(value));
            }break;
            case FACTOR:{
                if(Pattern.matches("[.0-9]+", value)) sizeValueKMLBubble =  trim(0.00000001,1.0,Double.valueOf(value));
            }break;
            default:sizeValueKMLBubble = null;
        }
    }
    
    private double trim(double min, double max, double value){
        if(value > max) return max;
        if(value < min) return min;
        return value;
    }
}
