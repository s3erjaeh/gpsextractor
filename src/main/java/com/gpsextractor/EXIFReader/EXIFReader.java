/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gpsextractor.EXIFReader;

import com.gpsextractor.imageResizer.ImageResizerTask;
import com.gpsextractor.imageResizer.SizeType;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.concurrent.Task;
import javafx.event.EventHandler;

/**
 *
 * @author Trader
 */
public class EXIFReader {
    
    private static final int numTask = Runtime.getRuntime().availableProcessors();
    private StringProperty lastFileName = new SimpleStringProperty("");
    private DoubleProperty progressProperty = new SimpleDoubleProperty();
    private double counter = 0;
    private int listSize = 0;
    private EventHandler successHandler;
    private ExecutorService imageResizer;
    
    public void readEXIF(Collection<ImageInfo> images, File kmlFile){
        
        counter = 0;
        listSize = images.size();
        progressProperty.set(0);
        
        BlockingQueue queue = new ArrayBlockingQueue(images.size());
        imageResizer = new ThreadPoolExecutor(numTask, numTask*4, 1, TimeUnit.MINUTES, queue){

            @Override
            protected void terminated() {
                super.terminated(); //To change body of generated methods, choose Tools | Templates.
                if(null != successHandler){
                    successHandler.handle(null);
                }
            }

        };
        for (ImageInfo image : images) {
            Task t = new EXIFReaderTask(image);
            t.setOnSucceeded(event -> {
                taskSuccess();
            });
            t.setOnScheduled(event -> {
                lastFileName.setValue(t.getTitle());
            });
            imageResizer.submit(t);
        }
        imageResizer.shutdown();
    }

    private synchronized void taskSuccess(){
        counter++;
        progressProperty.setValue(counter/listSize);
    }
    
    public void setOnSuccess(EventHandler handler){
        successHandler = handler;
    }
    
    public ReadOnlyStringProperty currentFileStringProperty() {
        return lastFileName;
    }

    public ReadOnlyDoubleProperty getProgressProperty() {
        return progressProperty;
    }
    
    public void cancle(){
        if(null != imageResizer){
            imageResizer.shutdownNow();
        }
    }
}
