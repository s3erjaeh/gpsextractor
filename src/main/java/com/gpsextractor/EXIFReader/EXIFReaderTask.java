/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gpsextractor.EXIFReader;

/**
 *
 * @author Trader
 */
import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.GpsDirectory;
import com.drew.metadata.exif.makernotes.PanasonicMakernoteDescriptor;
import com.drew.metadata.exif.makernotes.PanasonicMakernoteDirectory;
import java.io.File;
import javafx.concurrent.Task;
public class EXIFReaderTask  extends Task<ImageInfo>{
    
    private ImageInfo image;
    
    
    public EXIFReaderTask(ImageInfo image) {
        this.image = image;
    }

    @Override
    protected ImageInfo call() throws Exception {

        Metadata metadata = ImageMetadataReader.readMetadata(image.getFile());
        
        GpsDirectory directory = metadata.getFirstDirectoryOfType(GpsDirectory.class);
        if(directory != null)image.setLocation(directory.getGeoLocation());
                
        PanasonicMakernoteDirectory panasonicDirectory = metadata.getFirstDirectoryOfType(PanasonicMakernoteDirectory.class);
        if(panasonicDirectory == null) return image;
        
        PanasonicMakernoteDescriptor panasonicDescriptor = new PanasonicMakernoteDescriptor(panasonicDirectory);
        
        image.setLand(panasonicDescriptor.getDescription(PanasonicMakernoteDirectory.TAG_COUNTRY));
        image.setLandkreis(panasonicDescriptor.getDescription(PanasonicMakernoteDirectory.TAG_STATE));
        image.setOrt(panasonicDescriptor.getDescription(PanasonicMakernoteDirectory.TAG_CITY));
        image.setReisetag(panasonicDescriptor.getDescription(PanasonicMakernoteDirectory.TAG_TRAVEL_DAY));
        image.setSehenswürdigkeit(panasonicDescriptor.getDescription(PanasonicMakernoteDirectory.TAG_LANDMARK));
        
        return image;
    }

}
