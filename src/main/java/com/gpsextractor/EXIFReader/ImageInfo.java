/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gpsextractor.EXIFReader;

import com.drew.lang.GeoLocation;
import java.io.File;

/**
 *
 * @author Admin
 */
public class ImageInfo {
    private File file;
    private File previewFile;
    
    private Integer imageWidth;
    private Integer imageHeight;
    
    private GeoLocation location;
    
    private String land;
    private String landkreis;
    private String ort;
    private String sehenswürdigkeit;
    
    private String reisetag;

    public ImageInfo() {
    }

    public String getName(){
        return file.getName();
    }
    
    public ImageInfo(File file) {
        this.file = file;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public File getPreviewFile() {
        return previewFile;
    }

    public void setPreviewFile(File previewFile) {
        this.previewFile = previewFile;
    }

    public Integer getImageWidth() {
        return imageWidth;
    }

    public void setImageWidth(Integer imageWidth) {
        this.imageWidth = imageWidth;
    }

    public Integer getImageHeight() {
        return imageHeight;
    }

    public void setImageHeight(Integer imageHeight) {
        this.imageHeight = imageHeight;
    }

    public GeoLocation getLocation() {
        return location;
    }

    public void setLocation(GeoLocation location) {
        this.location = location;
    }

    public String getLand() {
        return land;
    }

    public void setLand(String land) {
        this.land = land;
    }

    public String getLandkreis() {
        return landkreis;
    }

    public void setLandkreis(String landkreis) {
        this.landkreis = landkreis;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public String getSehenswürdigkeit() {
        return sehenswürdigkeit;
    }

    public void setSehenswürdigkeit(String sehenswürdigkeit) {
        this.sehenswürdigkeit = sehenswürdigkeit;
    }

    public String getReisetag() {
        return reisetag;
    }

    public void setReisetag(String reisetag) {
        this.reisetag = reisetag;
    }

    


}