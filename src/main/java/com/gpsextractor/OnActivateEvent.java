/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gpsextractor;

import javafx.event.Event;
import javafx.event.EventType;

/**
 *
 * @author Trader
 */
public class OnActivateEvent extends Event{
    
     public static final EventType<OnActivateEvent> ACTIVATE = new EventType(ANY, "ACTIVATE");

    public OnActivateEvent() {
        super(ACTIVATE);
    }
    
}
