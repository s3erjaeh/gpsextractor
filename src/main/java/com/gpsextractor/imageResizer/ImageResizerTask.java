/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gpsextractor.imageResizer;

import com.gpsextractor.EXIFReader.ImageInfo;
import java.awt.image.BufferedImage;
import java.io.File;
import javafx.concurrent.Task;
import javax.imageio.ImageIO;
import static org.imgscalr.Scalr.*;

/**
 *
 * @author Trader
 */
public class ImageResizerTask extends Task<ImageInfo>{

    private ImageInfo image;
    private double factor;
    private final File destinationFile;
    private SizeType type;
    private final boolean overrideExisting;

    public ImageResizerTask(ImageInfo image, SizeType type, double factor, File destinationDirectory,Boolean overrideExisting) {
        this.image = image;
        this.factor = factor;
        this.type = type;
        this.overrideExisting = overrideExisting;
        this.destinationFile = new File(destinationDirectory.getPath() + "/" + image.getFile().getName());
        this.image.setPreviewFile(destinationFile);
        updateTitle(image.getFile().getName());
    }

    @Override
    protected ImageInfo call() throws Exception {
        
        
        BufferedImage img = ImageIO.read(image.getFile());
        image.setImageWidth(img.getWidth());
        image.setImageHeight(img.getHeight());
        
        if(destinationFile.exists() && !overrideExisting){
            return image;
        }
        
        int newHeight = img.getHeight();
        int newWidth  =img.getWidth();
        switch (type){
            case PROZENT:{
                factor /= 100.0;
            }
            case FACTOR:{
                newHeight *= factor;
                newWidth *= factor;
            }break;
            case ABSOLUTE_HEIGHT: {
                newHeight = (int) factor;
                newWidth *= ((double) newHeight / (double) img.getHeight());
            } break;
            case ABSOLUTE_WIDTH:{
                newWidth = (int) factor;
                newHeight *= ((double) newWidth / (double) img.getWidth());
            }break;
        }
        img =  resize(img, Method.SPEED, newWidth,newHeight);
        
        ImageIO.write(img, "jpg", destinationFile);
        if (isCancelled()) {
            return image;
        }
        return image;
    }    
    
}
