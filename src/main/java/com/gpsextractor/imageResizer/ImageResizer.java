/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gpsextractor.imageResizer;

import com.gpsextractor.EXIFReader.ImageInfo;
import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.concurrent.Task;
import javafx.event.EventHandler;

/**
 *
 * @author Trader
 */
public class ImageResizer{

    private static final int numTask = Runtime.getRuntime().availableProcessors();
    private StringProperty lastFileName = new SimpleStringProperty("");
    private DoubleProperty progressProperty = new SimpleDoubleProperty();
    private double counter = 0;
    private int listSize = 0;
    private EventHandler successHandler;
    private ExecutorService imageResizer;
    
    public ImageResizer() {
    }
    
    public void resizeImages(List<ImageInfo> images, File destinationDirectory, SizeType type, Double factor, Boolean overrideExisting){
        
        listSize = images.size();
        BlockingQueue queue = new ArrayBlockingQueue(images.size());
        imageResizer = new ThreadPoolExecutor(numTask, numTask*4, 1, TimeUnit.MINUTES, queue){

            @Override
            protected void terminated() {
                super.terminated(); //To change body of generated methods, choose Tools | Templates.
                if(null != successHandler){
                    successHandler.handle(null);
                }
            }
        };
        for (ImageInfo image : images) {
            Task t = new ImageResizerTask(image, type, factor, destinationDirectory,overrideExisting);
            t.setOnSucceeded(event -> {
                taskSuccess();
            });
            t.setOnScheduled(event -> {
                lastFileName.setValue(t.getTitle());
            });
            imageResizer.submit(t);
        }
        imageResizer.shutdown();
    }

    private synchronized void taskSuccess(){
        counter++;
        progressProperty.setValue(counter/listSize);
    }
    
    public void setOnSuccess(EventHandler handler){
        successHandler = handler;
    }
    
    public ReadOnlyStringProperty currentFileStringProperty() {
        return lastFileName;
    }

    public ReadOnlyDoubleProperty getProgressProperty() {
        return progressProperty;
    }
    
    public void cancle(){
        if(null != imageResizer){
            imageResizer.shutdownNow();
        }
    }
}
