/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gpsextractor.imageResizer;

/**
 *
 * @author Trader
 */
public enum SizeType{

    PROZENT("Prozent","%"),
    ABSOLUTE_HEIGHT("Absolute Höhe","px"),
    ABSOLUTE_WIDTH("Absolute Breite","px"),
    FACTOR("Faktor","");        

    private final String name;
    private final String unit;
    SizeType(String name,String unit){
        this.name = name;
        this.unit = unit;
    }

    @Override
    public String toString() {
        return name + ':';
    }

    public String getUnit(){
        return this.unit;
    }
}